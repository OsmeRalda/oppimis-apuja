TS ja JS komennot

let firstName: string ="Dylan";  //määritellään property johonkin
----------------------------------------------------------------------------------
Anonyymi funktio
					
let Sum = function(x: number, y: number) : number //function() sisällä parametrien tyypit ja : jälkeen palautus tyyppi!!
{
    return x + y;
}

Sum(2,3); // returns 5
------------------------------------------------------------------------------------
type tamaObjecti {
asia1: number;
asia2: string;
asia3?: boolean;				// ? tarkoittaa että asia ei ole pakollinen 
}

const muuttuja: tamaObjecti;
------------
function muuttuja(asia:tamaObjecti): void{}
---------------------------------------------------------------------------------
Parametritön Arrow funktio
let Print = () => console.log("Hello TypeScript");

Print(); //Output: Hello TypeScript
----------------------------------------------------------------------------------
Arrow funktio luokassa
class Employee {
    empCode: number;
    empName: string;

    constructor(code: number, name: string) {
        this.empName = name;
        this.empCode = code;
    }

    display = () => console.log(this.empCode +' ' + this.empName)
}
let emp = new Employee(1, 'Ram');
emp.display();
----------------------------------------------------------------------------------
Array
const names: string[] = [];   //luodaan array
names.push("Dylan");			// jonne laitetaan property
----------------------------------------------------------------------------------
tuple on tyypitetty array jossa on ennalta määritetty pituus ja tyypit jokaiselle indexille.
//määritä tuple

let tamaTuple: [number, boolean, string};

//alusta oikein

tamaTuple = [5, false, 'Jokin kiva teksti tähän']; //jos arvot ovat väärässä järjestyksessä tulee error.

//hyvä tapa on tehdä tuplet readonlyksi
//nimetyt tuplet antaa hieman osviittaa että mikä tietty indexi on

const graph: [x: number, y: number] = [55.2, 43.2];

//tuplet voi myös purkaa

const graph: [x: number, y: number] = [55.2, 43.2];
const [x,y] = graph;
-------------------------------------------------------------------------------------
map()
const lukujono = [20, 22, 20.5, 19, 21, 21.5, 23];

const lukujonoJohonLisatty = lukujono.map(jono => jono + 1.5);				//.map ottaa jokaisen yksittäisen arrayn osan
										// '=>' korvaa käytännösssä function. eli jono => jono + 1.5 tarkoittaa että jokaiseen arrayn osaan lisätään 1.5
console.log(lukujonoJohonLisatty); // palauttaa [ 21.5, 23.5, 22, 20.5, 22.5, 23, 24.5 ]
------------------------------------------------------------------------------------
filter()
function dayTemperature(element, index, array) { 
   return (element <= 20); 
} 
          
const weeklyReadings = [20, 22, 20.5, 19, 21, 21.5, 23].filter(dayTemperature); 
console.log("Alle 20c päiviä : " + weeklyReadings );
-------------------------------------------------------------------------------------
if
let x: number = 10, y = 20;

if (x < y) 
{
    console.log('x is less than y');
} 
------------------------------------------------------------------------------------
if else
let let x: number = 10, y = 20;

if (x > y) 
{
    console.log('x is greater than y.');
} 
else
{
    console.log('x is less than or equal to y.'); //This will be executed
}
--------------------------------------------------------------------------------------
if else   ? ternary operatorilla
let x: number = 10, y = 20;

x > y? console.log('x is greater than y.'): console.log('x is less than or equal to y.')
------------------------------------------------------------------------------------
else if
let x: number = 10, y = 20;

if (x > y) 
{
    console.log('x is greater than y.');
} 
else if (x < y)
{
    console.log('x is less than y.'); //This will be executed
}
else if (x == y) 
{
    console.log('x is equal to y');
}
--------------------------------------------------------------------------------------
Type alias  voit määrittää asioita custom nimellä. voi käyttää stringille, numberille, objectille tai arraylle           

type CarYear = number
type CarType = string
type CarModel = string
type Car = {
  year: CarYear,
  type: CarType,
  model: CarModel
}

const carYear: CarYear = 2001
const carType: CarType = "Toyota"
const carModel: CarModel = "Corolla"
const car: Car = {
  year: carYear,
  type: carType,
  model: carModel
};
-----------------------------------------------------------------------------------
				interface käytännössä sama kuin typetys mutta toimii ainoastaan objecteihin.
interface Rectangle {
  height: number,
  width: number
}

const rectangle: Rectangle = {
  height: 20,
  width: 10
};